﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Notes2.Models
{
    public class Notes2Context : DbContext
    {
        public Notes2Context (DbContextOptions<Notes2Context> options)
            : base(options)
        {
        }

        public DbSet<Notes2.Models.Note> Note { get; set; }

        public override int SaveChanges()
        {
            var newEntities = this.ChangeTracker.Entries()
        .Where(
            x => x.State == EntityState.Added &&
            x.Entity != null &&
            x.Entity as TimeStampedModel != null
            )
        .Select(x => x.Entity as TimeStampedModel);

            var modifiedEntities = this.ChangeTracker.Entries()
                .Where(
                    x => x.State == EntityState.Modified &&
                    x.Entity != null &&
                    x.Entity as TimeStampedModel != null
                    )
                .Select(x => x.Entity as TimeStampedModel);

            foreach (var newEntity in newEntities)
            {
                newEntity.created = DateTime.UtcNow;
                newEntity.updated = DateTime.UtcNow;
            }

            foreach (var modifiedEntity in modifiedEntities)
            {
                modifiedEntity.updated = DateTime.UtcNow;
            }
            return base.SaveChanges();
        }
    }
}
