﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notes2.Models
{
    public class TimeStampedModel
    {
        public DateTime created { get; set; }
        public DateTime updated { get; set; }
    }
}
